package com.nimbusds.infinispan.persistence.dynamodb.config;


import java.util.Arrays;
import java.util.HashSet;

import junit.framework.TestCase;


public class DynamoDBStoreConfigurationParserTest extends TestCase {
	

	public void testSplit() {
	
		assertEquals(new HashSet<>(Arrays.asList("a", "b", "c")), DynamoDBStoreConfigurationParser13.parseStringSet("a, b, c"));
		assertEquals(new HashSet<>(Arrays.asList("a", "b", "c")), DynamoDBStoreConfigurationParser13.parseStringSet("a,b,c"));
		assertEquals(new HashSet<>(Arrays.asList("a", "b", "c")), DynamoDBStoreConfigurationParser13.parseStringSet("a b c"));
		assertEquals(new HashSet<>(Arrays.asList("a", "b", "c")), DynamoDBStoreConfigurationParser13.parseStringSet("a , b,  c"));
	}
}
