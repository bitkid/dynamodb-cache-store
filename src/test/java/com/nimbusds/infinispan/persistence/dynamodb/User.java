package com.nimbusds.infinispan.persistence.dynamodb;


import java.time.Instant;
import java.util.Set;

import net.jcip.annotations.Immutable;
import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * User POJO, test class.
 */
@Immutable
public final class User {
	
	
	private final String name;
	
	
	private final String email;
	
	
	private final Instant created;
	
	
	private final Set<String> permissions;
	
	
	public User(final String name, final String email) {
		this(name, email, null, null);
	}
	
	
	public User(final String name, final String email, final Instant created, final Set<String> permissions) {
		this.name = name;
		this.email = email;
		this.created = created;
		this.permissions = permissions;
	}
	
	
	public String getName() {
		return name;
	}
	
	
	public String getEmail() {
		return email;
	}
	
	
	public Instant getCreated() {
		return created;
	}
	
	
	public Set<String> getPermissions() {
		return permissions;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;
		
		User user = (User) o;
		
		if (name != null ? !name.equals(user.name) : user.name != null)
			return false;
		if (email != null ? !email.equals(user.email) : user.email != null)
			return false;
		if (created != null ? !created.equals(user.created) : user.created != null)
			return false;
		return permissions != null ? permissions.equals(user.permissions) : user.permissions == null;
		
	}
	
	
	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (created != null ? created.hashCode() : 0);
		result = 31 * result + (permissions != null ? permissions.hashCode() : 0);
		return result;
	}
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
			.append("name", name)
			.append("email", email)
			.append("created", created)
			.append("permissions", permissions)
			.toString();
	}
}
