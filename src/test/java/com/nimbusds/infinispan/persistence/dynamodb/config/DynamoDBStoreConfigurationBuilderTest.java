package com.nimbusds.infinispan.persistence.dynamodb.config;


import com.amazonaws.regions.Regions;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.cache.PersistenceConfigurationBuilder;
import org.junit.Test;

import static org.junit.Assert.*;


public class DynamoDBStoreConfigurationBuilderTest {
	

	@Test
	public void testCreateDefault() {
		
		PersistenceConfigurationBuilder persistenceConfigurationBuilder = new ConfigurationBuilder().persistence();
		
		DynamoDBStoreConfigurationBuilder b = new DynamoDBStoreConfigurationBuilder(persistenceConfigurationBuilder);
		
		DynamoDBStoreConfiguration config = b.create();
		
		// Infinispan
		assertFalse(config.purgeOnStartup());
		assertFalse(config.fetchPersistentState());
		assertFalse(config.ignoreModifications());
		assertNotNull(config.async());
		assertFalse(config.async().enabled());
		assertNotNull(config.singletonStore());
		assertFalse(config.singletonStore().enabled());
		assertFalse(config.preload());
		assertFalse(config.shared());
		assertNull(config.getEndpoint());
		assertNull(config.getMetricRegistry());
		assertEquals(Regions.DEFAULT_REGION, config.getRegion());
		assertNull(config.getItemTransformerClass());
		assertNull(config.getQueryExecutorClass());
		assertEquals(1L, config.getProvisionedThroughput().getReadCapacityUnits().longValue());
		assertEquals(1L, config.getProvisionedThroughput().getWriteCapacityUnits().longValue());
		assertFalse(config.isTableWithEncryptionAtRest());
		assertEquals("", config.getTablePrefix());
		assertNull(config.getApplyRangeKey());
		assertNull(config.getRangeKeyValue());
		assertFalse(config.isEnableStream());
		assertFalse(config.isEnableContinuousBackups());
	}
}
