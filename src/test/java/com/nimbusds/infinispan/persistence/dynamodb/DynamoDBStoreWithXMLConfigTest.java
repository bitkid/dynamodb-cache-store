package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.regions.Regions;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;
import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;


/**
 * Tests the DynamoDB store with an XML-based config.
 */
public class DynamoDBStoreWithXMLConfigTest extends DynamoDBStoreWithProgConfigTest {
	
	
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();

		setUpLocalClient();

		cacheMgr = new DefaultCacheManager("test-infinispan.xml");

		cacheMgr.start();
		
		assertFalse(((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).isTableWithEncryptionAtRest());
	}
	
	
	public void testSharedEnabled() {
		
		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertTrue(myMap.getCacheConfiguration().persistence().stores().get(0).shared());
		
		assertEquals(0, myMap.size());
	}
	
	
	public void testWithRegionSpec()
		throws Exception {
		
		cacheMgr.stop();
		
		cacheMgr = new DefaultCacheManager("test-infinispan-with-region-spec.xml");
		
		cacheMgr.start();
		
		assertEquals(Regions.EU_CENTRAL_1, ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).getRegion());
	}
	
	
	public void testWithTablePrefix()
		throws Exception {
		
		cacheMgr.stop();
		
		cacheMgr = new DefaultCacheManager("test-infinispan-table-prefix.xml");
		
		cacheMgr.start();
		
		assertEquals("myapp_", ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).getTablePrefix());
	}
	
	
	public void testWithEncryptionAtRest()
		throws Exception {
		
		cacheMgr.stop();
		
		cacheMgr = new DefaultCacheManager("test-infinispan-encryption-at-rest.xml");
		
		cacheMgr.start();
		
		assertTrue(((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).isTableWithEncryptionAtRest());
	}
}