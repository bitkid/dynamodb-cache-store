package com.nimbusds.infinispan.persistence.dynamodb;


import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import org.infinispan.metadata.InternalMetadata;


class ExpiringUserItemTransformer extends UserItemTransformer {
	
	
	@Override
	public InfinispanEntry<String,User> toInfinispanEntry(final Item item) {
		
		InfinispanEntry<String,User> baseEntry = super.toInfinispanEntry(item);
		
		final Instant now = Instant.now();
		final Date yesterday = Date.from(now.minus(1L, ChronoUnit.DAYS));
		
		// Simulate expired entry
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(yesterday)
			.lastUsed(yesterday)
			.lifespan(6L, TimeUnit.HOURS)
			.maxIdle(1L, TimeUnit.HOURS)
			.build();
		
		assert metadata.isExpired(now.toEpochMilli());
		
		return new InfinispanEntry<>(baseEntry.getKey(), baseEntry.getValue(), metadata);
	}
}