package com.nimbusds.infinispan.persistence.dynamodb;


import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.Item;
import junit.framework.TestCase;
import org.junit.Assert;


public class ItemSanitizationTest extends TestCase {
	
	
	public void testSanitizeItem_stringOk() {
		
		Item item = new Item().withString("name", "Alice");
		
		item = ItemSanitization.sanitize(item);
		
		assertEquals("Alice", item.getString("name"));
		assertEquals(1, item.numberOfAttributes());
	}
	
	
	public void testSanitizeItem_byteArrayOk() {
		
		Item item = new Item().withBinary("data", new byte[]{0,1,2,3});
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertArrayEquals(new byte[]{0, 1, 2, 3}, item.getBinary("data"));
		assertEquals(1, item.numberOfAttributes());
	}
	
	
	public void testSanitizeItem_byteBufferOk() {
		
		Item item = new Item().withBinary("data", ByteBuffer.wrap(new byte[]{0,1,2,3}));
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertArrayEquals(new byte[]{0, 1, 2, 3}, item.getByteBuffer("data").array());
		assertEquals(1, item.numberOfAttributes());
	}
	
	
	public void testSanitizeItem_stringSetOk() {
		
		Item item = new Item().withStringSet("names", new HashSet<>(Arrays.asList("Alice", "Bob")));
		
		item = ItemSanitization.sanitize(item);
		
		assertEquals(new HashSet<>(Arrays.asList("Alice", "Bob")), item.getStringSet("names"));
		assertEquals(1, item.numberOfAttributes());
	}
	
	
	public void testSanitizeItem_mapOk() {
		
		Map<String,Object> map = new HashMap<>();
		map.put("name", "Alice");
		map.put("emails", new HashSet<>(Arrays.asList("alice@example.com", "alice@example.org")));
		
		Item item = new Item().withMap("attrs", map);
		
		assertEquals("Alice", item.getMap("attrs").get("name"));
		assertEquals(new HashSet<>(Arrays.asList("alice@example.com", "alice@example.org")), item.getMap("attrs").get("emails"));
		assertEquals(2, item.getMap("attrs").size());
		assertEquals(1, item.numberOfAttributes());
	}
	
	
	public void testSanitizeItem_removeEmptyString() {
		
		Item item = new Item().withString("name", "");
		
		item = ItemSanitization.sanitize(item);
		
		assertEquals(0, item.numberOfAttributes());
	}
	
	
	public void testSanitizeItem_removeEmptyByteArray() {
		
		Item item = new Item().withBinary("data", new byte[]{});
		
		item = ItemSanitization.sanitize(item);
		
		assertEquals(0, item.numberOfAttributes());
	}
	
	
	public void testSanitizeItem_removeEmptyByteBuffer() {
		
		Item item = new Item().withBinary("data", ByteBuffer.wrap(new byte[]{}));
		
		item = ItemSanitization.sanitize(item);
		
		assertEquals(0, item.numberOfAttributes());
	}
	
	
	public void testSanitizeItem_removeEmptyString_removeEmptySet() {
		
		Item item = new Item()
			.withString("name", "")
			.withStringSet("names", new HashSet<>());
		
		item = ItemSanitization.sanitize(item);
		
		assertEquals(0, item.numberOfAttributes());
	}
	
	
	public void testSanitizeItem_removeEmptyString_removeEmptySet_keepInteger() {
		
		Item item = new Item()
			.withInt("age", 21)
			.withString("name", "")
			.withStringSet("names", new HashSet<>());
		
		item = ItemSanitization.sanitize(item);
		
		assertEquals(21, item.getInt("age"));
		assertEquals(1, item.numberOfAttributes());
	}
	
	
	public void testSanitizeItem_nestedMap_removeEmptyString_removeEmptySet() {
		
		Map<String,Object> map = new HashMap<>();
		map.put("name", "Alice");
		map.put("emails", new HashSet<>(Arrays.asList("alice@example.com", "alice@example.org")));
		map.put("address", "");
		map.put("locations", new HashSet<String>());
		
		Item item = new Item().withMap("attrs", map);
		
		item = ItemSanitization.sanitize(item);
		
		assertEquals("Alice", item.getMap("attrs").get("name"));
		assertEquals(new HashSet<>(Arrays.asList("alice@example.com", "alice@example.org")), item.getMap("attrs").get("emails"));
		assertEquals(2, item.getMap("attrs").size());
		assertEquals(1, item.numberOfAttributes());
	}
	
	
	public void testFromJSONObject_sanitize() {
		String json = "{\n" +
			" \"name\" : \"\",\n" +
			" \"locations\" : [],\n" +
			" \"empty_data\" : {},\n" +
			" \"nested_data\" : { \"key-1\" : \"\", \"key-2\" : [] }\n" +
			"}";
		Item item = Item.fromJSON(json);
		item = ItemSanitization.sanitize(item);
		assertTrue(item.getMap("empty_data").isEmpty());
		assertTrue(item.getMap("nested_data").isEmpty());
		assertEquals(2, item.numberOfAttributes());
	}
}
