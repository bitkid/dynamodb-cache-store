package com.nimbusds.infinispan.persistence.dynamodb.config;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;


public class AttributeTest {
	

	@Test
	public void testLocalNames() {
		
		assertNull(Attribute.UNKNOWN.getLocalName());
		assertEquals("endpoint", Attribute.ENDPOINT.getLocalName());
		assertEquals("region", Attribute.REGION.getLocalName());
		assertEquals("item-transformer", Attribute.ITEM_TRANSFORMER.getLocalName());
		assertEquals("query-executor", Attribute.QUERY_EXECUTOR.getLocalName());
		assertEquals("indexed-attributes", Attribute.INDEXED_ATTRIBUTES.getLocalName());
		assertEquals("read-capacity", Attribute.READ_CAPACITY.getLocalName());
		assertEquals("write-capacity", Attribute.WRITE_CAPACITY.getLocalName());
		assertEquals("encryption-at-rest", Attribute.ENCRYPTION_AT_REST.getLocalName());
		assertEquals("table-prefix", Attribute.TABLE_PREFIX.getLocalName());
		assertEquals("apply-range-key", Attribute.APPLY_RANGE_KEY.getLocalName());
		assertEquals("range-key-value", Attribute.RANGE_KEY_VALUE.getLocalName());
		assertEquals("enable-stream", Attribute.ENABLE_STREAM.getLocalName());
		assertEquals("enable-continuous-backups", Attribute.ENABLE_CONTINUOUS_BACKUPS.getLocalName());
		
		assertEquals(14, Attribute.values().length);
	}
}
