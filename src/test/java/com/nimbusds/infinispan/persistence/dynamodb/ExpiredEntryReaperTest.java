package com.nimbusds.infinispan.persistence.dynamodb;


import java.time.Duration;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.amazonaws.services.dynamodbv2.document.BatchWriteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import org.infinispan.marshall.core.MarshalledEntry;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;


public class ExpiredEntryReaperTest extends TestWithDynamoDB {
	
	
	static final long READ_CAPACITY = 100L;
	
	
	static final long WRITE_CAPACITY = 100L;
	
	
	public void testExpireWithKeyListener()
		throws Exception {
		
		RequestFactory<String,User> requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
			"",
			null,
			null,
			false);
		
		// Create table
		Instant startTs = Instant.now();
		
		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());
		
		table.waitForActive();
		
		System.out.println("Created table " + table.getTableName() + ": " + Duration.between(startTs, Instant.now()));
		
		startTs = Instant.now();
		
		for (int i=0; i < 2000; ++i) {
			
			User user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			table.putItem(requestFactory.resolveItem(new InfinispanEntry<>(i + "", user, null)));
		}
		
		System.out.println("Added 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		startTs = Instant.now();
		
		AtomicInteger counter = new AtomicInteger(0);
		table.scan().forEach(item -> counter.incrementAndGet());
		assertEquals(2000, counter.get());
		
		System.out.println("Scanned 2000 items: " + Duration.between(startTs, Instant.now()));
		
		ExpiredEntryReaper<String,User> reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory);
		
		List<String> deletedKeys = new LinkedList<>();
		
		startTs = Instant.now();
		
		// Purge expired entries (all must be marked as such)
		assertEquals(2000, reaper.purge(key -> {
			assertNotNull(key);
			deletedKeys.add(key);
		}));
		
		System.out.println("Purged 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}
		
		assertEquals(2000, deletedKeys.size());
	}
	
	
	public void testExpireWithEntryListener()
		throws Exception {
		
		RequestFactory<String,User> requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
			"",
			null,
			null,
			false);
		
		// Create table
		Instant startTs = Instant.now();
		
		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());
		
		table.waitForActive();
		
		System.out.println("Created table " + table.getTableName() + ": " + Duration.between(startTs, Instant.now()));
		
		startTs = Instant.now();
		
		for (int i=0; i < 2000; ++i) {
			
			User user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			table.putItem(requestFactory.resolveItem(new InfinispanEntry<>(i + "", user, null)));
		}
		
		System.out.println("Added 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		startTs = Instant.now();
		
		AtomicInteger counter = new AtomicInteger(0);
		table.scan().forEach(item -> counter.incrementAndGet());
		assertEquals(2000, counter.get());
		
		System.out.println("Scanned 2000 items: " + Duration.between(startTs, Instant.now()));
		
		ExpiredEntryReaper<String,User> reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory);
		
		List<String> deletedKeys = new LinkedList<>();
		
		startTs = Instant.now();
		
		// Purge expired entries (all must be marked as such)
		assertEquals(2000, reaper.purgeExtended(
			new AdvancedCacheExpirationWriter.ExpirationPurgeListener<String, User>() {
				@Override
				public void marshalledEntryPurged(MarshalledEntry<String, User> marshalledEntry) {
					deletedKeys.add(marshalledEntry.getKey());
					assertNotNull(marshalledEntry.getValue());
					assertNotNull(marshalledEntry.getMetadata());
				}
				
				
				@Override
				public void entryPurged(String s) {
					fail(); // never called
				}
			}));
		
		System.out.println("Purged 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}
		
		assertEquals(2000, deletedKeys.size());
	}
	
	
	static class BatchQueue25 extends LinkedList<Item> {
		
		List<Item> pollBatch() {
		
			List<Item> batch = new LinkedList<>();
			
			while(peek() != null && batch.size() <= 25) {
				batch.add(poll());
			}
			
			return batch.isEmpty() ? null : batch;
		}
	}
	
	
	private void batchPut(final Table table, final BatchQueue25 queue) {
		
		while (queue.peek() != null) {
			
			TableWriteItems writeItems = new TableWriteItems(table.getTableName())
				.withItemsToPut(queue.pollBatch());
			
			BatchWriteItemOutcome outcome = dynamoDB.batchWriteItem(writeItems);
			
			do {
				Map<String,List<WriteRequest>> unprocessedItems = outcome.getUnprocessedItems();
				
				if (outcome.getUnprocessedItems().size() > 0) {
					System.out.println("Retrieving unprocessed items...");
					outcome = dynamoDB.batchWriteItemUnprocessed(unprocessedItems);
				}
				
			} while (outcome.getUnprocessedItems().size() > 0);
		}
	}
	
	
	public void testExpire_withBatchAdd()
		throws Exception {
		
		RequestFactory<String,User> requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
			"",
			null,
			null,
			false);
		
		// Create table
		Instant startTs = Instant.now();
		
		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());
		
		table.waitForActive();
		
		System.out.println("Created table " + table.getTableName() + ": " + Duration.between(startTs, Instant.now()));
		
		startTs = Instant.now();
		
		BatchQueue25 users = new BatchQueue25();
		
		for (int i=0; i < 2000; ++i) {
			
			User user = new User("Alice " + i, "hello-" + i + "@email.com");
			users.add(requestFactory.resolveItem(new InfinispanEntry<>(i + "", user, null)));
			batchPut(table, users);
		}
		
		System.out.println("Added 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		startTs = Instant.now();
		
		AtomicInteger counter = new AtomicInteger(0);
		table.scan().forEach(item -> counter.incrementAndGet());
		assertEquals(2000, counter.get());
		
		System.out.println("Scanned 2000 items: " + Duration.between(startTs, Instant.now()));
		
		ExpiredEntryReaper<String,User> reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory);
		
		List<String> deletedKeys = new LinkedList<>();
		
		startTs = Instant.now();
		
		// Purge expired entries (all must be marked as such)
		assertEquals(2000, reaper.purge(key -> {
			assertNotNull(key);
			deletedKeys.add(key);
		}));
		
		System.out.println("Purged 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}
		
		assertEquals(2000, deletedKeys.size());
	}
	
	
	public void testExpire_withRangeKey()
		throws Exception {
		
		RequestFactory<String,User> requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
			"",
			"tid",
			"123",
			false);
		
		// Create table
		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());
		
		table.waitForActive();
		
		for (int i=0; i < 2000; ++i) {
			
			User user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			table.putItem(requestFactory.resolveItem(new InfinispanEntry<>(i + "", user, null)));
		}
		
		AtomicInteger counter = new AtomicInteger(0);
		table.scan().forEach(item -> counter.incrementAndGet());
		assertEquals(2000, counter.get());
		
		
		ExpiredEntryReaper<String,User> reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory);
		
		List<String> deletedKeys = new LinkedList<>();
		
		// Purge expired entries (all must be marked as such)
		assertEquals(2000, reaper.purge(key -> {
			assertNotNull(key);
			deletedKeys.add(key);
		}));
		
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}
		
		assertEquals(2000, deletedKeys.size());
	}
	
	
	public void testExpire_withTableNamePrefix()
		throws Exception {
		
		RequestFactory<String,User> requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
			"prefix-",
			null,
			null,
			false);
		
		// Create table
		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());
		
		table.waitForActive();
		
		assertEquals("prefix-test-users", table.getTableName());
		
		for (int i=0; i < 2000; ++i) {
			
			User user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			table.putItem(requestFactory.resolveItem(new InfinispanEntry<>(i + "", user, null)));
		}
		
		AtomicInteger counter = new AtomicInteger(0);
		table.scan().forEach(item -> counter.incrementAndGet());
		assertEquals(2000, counter.get());
		
		
		ExpiredEntryReaper<String,User> reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory);
		
		List<String> deletedKeys = new LinkedList<>();
		
		// Purge expired entries (all must be marked as such)
		assertEquals(2000, reaper.purge(key -> {
			assertNotNull(key);
			deletedKeys.add(key);
		}));
		
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}
		
		assertEquals(2000, deletedKeys.size());
	}
}
