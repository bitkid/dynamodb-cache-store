package com.nimbusds.infinispan.persistence.dynamodb;


import java.time.Instant;
import java.util.Set;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;


/**
 * Transforms User POJO to / from DynamoDB item.
 */
public class UserItemTransformer implements DynamoDBItemTransformer<String,User> {
	
	
	
	@Override
	public String getTableName() {
		
		return "test-users";
	}
	
	
	@Override
	public String getHashKeyAttributeName() {
		
		return "uid";
	}
	
	
	@Override
	public String resolveHashKey(final String key) {
		
		return key;
	}
	
	
	@Override
	public Item toItem(final InfinispanEntry<String,User> infinispanEntry) {
		
		Item item = new Item()
			.withPrimaryKey("uid", infinispanEntry.getKey())
			.with("given_name", infinispanEntry.getValue().getName().split("\\s")[0])
			.with("surname", infinispanEntry.getValue().getName().split("\\s")[1])
			.with("email", infinispanEntry.getValue().getEmail());
		
		if (infinispanEntry.getValue().getCreated() != null) {
			item.with("created", infinispanEntry.getValue().getCreated().toEpochMilli());
		}
		
		if (infinispanEntry.getValue().getPermissions() != null) {
			item.with("permissions", infinispanEntry.getValue().getPermissions());
		}
		
		return item;
	}
	
	
	@Override
	public InfinispanEntry<String,User> toInfinispanEntry(final Item item) {
		
		String uid = item.getString("uid");
		String givenName = item.getString("given_name");
		String surname = item.getString("surname");
		String name = givenName + " " + surname;
		String email = item.getString("email");
		
		Instant created = null;
		
		if (item.hasAttribute("created") && item.getLong("created") > 0L) {
			created = Instant.ofEpochMilli(item.getLong("created"));
		}
		
		Set<String> permissions = null;
		
		if (item.getStringSet("permissions") != null) {
			permissions = item.getStringSet("permissions");
		}
		
		return new InfinispanEntry<>(
			uid,
			new User(
				name,
				email,
				created,
				permissions),
			null);
	}
}
