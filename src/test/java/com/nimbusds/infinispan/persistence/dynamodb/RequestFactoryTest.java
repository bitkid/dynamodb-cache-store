package com.nimbusds.infinispan.persistence.dynamodb;


import java.util.Collections;
import java.util.Iterator;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.model.*;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import junit.framework.TestCase;


public class RequestFactoryTest extends TestCase {
	
	
	static class ItemTransformer implements DynamoDBItemTransformer<String,String> {
		@Override
		public String getTableName() {
			return "users";
		}
		
		
		@Override
		public String getHashKeyAttributeName() {
			return "uid";
		}
		
		
		@Override
		public String resolveHashKey(String key) {
			return key;
		}
		
		
		@Override
		public Item toItem(InfinispanEntry<String, String> infinispanEntry) {
			return new Item()
				.withPrimaryKey("uid", infinispanEntry.getKey())
				.with("email", infinispanEntry.getValue());
		}
		
		
		@Override
		public InfinispanEntry<String, String> toInfinispanEntry(Item item) {
			String uid = item.getString("uid");
			String email = item.getString("email");
			
			return new InfinispanEntry<>(
				uid,
				email,
				null);
		}
	}
	
	
	public void testBasic() {
		
		ItemTransformer tr = new ItemTransformer();
		
		RequestFactory<String,String> rf = new RequestFactory<>(
			tr,
			null,
			new ProvisionedThroughput(1L, 1L),
			false,
			"",
			null,
			null,
			false);
		
		assertFalse(rf.appliesRangeKey());
		
		assertEquals("users", rf.getTableName());
		assertEquals(tr.getTableName(), rf.getTableName());
		
		assertNull(rf.getRangeKeyGSIName());
		
		CreateTableRequest createTableRequest = rf.resolveCreateTableRequest();
		assertEquals("users", createTableRequest.getTableName());
		assertEquals("uid", createTableRequest.getKeySchema().get(0).getAttributeName());
		assertEquals("HASH", createTableRequest.getKeySchema().get(0).getKeyType());
		assertEquals(1, createTableRequest.getKeySchema().size());
		assertEquals("uid", createTableRequest.getAttributeDefinitions().get(0).getAttributeName());
		assertEquals("S", createTableRequest.getAttributeDefinitions().get(0).getAttributeType());
		assertEquals(1, createTableRequest.getAttributeDefinitions().size());
		assertEquals(1L, createTableRequest.getProvisionedThroughput().getReadCapacityUnits().longValue());
		assertEquals(1L, createTableRequest.getProvisionedThroughput().getWriteCapacityUnits().longValue());
		assertFalse(createTableRequest.getSSESpecification().isEnabled());
		assertNull(createTableRequest.getStreamSpecification());
		assertNull(createTableRequest.getGlobalSecondaryIndexes());
		assertNull(createTableRequest.getLocalSecondaryIndexes());
		
		GetItemSpec getItemSpec = rf.resolveGetItemSpec("123");
		assertEquals("uid", getItemSpec.getKeyComponents().iterator().next().getName());
		assertEquals("123", getItemSpec.getKeyComponents().iterator().next().getValue());
		assertEquals(1, getItemSpec.getKeyComponents().size());
		
		Item resolvedItem = rf.resolveItem(new InfinispanEntry<>(
			"123",
			"alice@wonderland.net",
			null));
		
		assertEquals("123", resolvedItem.getString("uid"));
		assertEquals("alice@wonderland.net", resolvedItem.getString("email"));
		assertEquals(2, resolvedItem.numberOfAttributes());
		
		DeleteItemSpec deleteItemSpec = rf.resolveDeleteItemSpec("123");
		assertEquals("uid", deleteItemSpec.getKeyComponents().iterator().next().getName());
		assertEquals("123", deleteItemSpec.getKeyComponents().iterator().next().getValue());
		assertEquals(1, deleteItemSpec.getKeyComponents().size());
		assertEquals(ReturnValue.ALL_OLD.name(), deleteItemSpec.getReturnValues());
		
		Item item = rf.applyOptionalRangeKey(resolvedItem);
		assertEquals("123", item.getString("uid"));
		assertEquals("alice@wonderland.net", item.getString("email"));
		assertEquals(2, item.numberOfAttributes());
	}
	
	
	public void testWithQueryExecutor() {
		
		ItemTransformer tr = new ItemTransformer();
		
		RequestFactory<String,String> rf = new RequestFactory<>(
			tr,
			Collections.singleton("email"),
			new ProvisionedThroughput(1L, 1L),
			false,
			"",
			null,
			null,
			false);
		
		assertFalse(rf.appliesRangeKey());
		
		assertEquals("users", rf.getTableName());
		assertEquals(tr.getTableName(), rf.getTableName());
		
		assertNull(rf.getRangeKeyGSIName());
		
		assertEquals("users-email-gsi", rf.getGSIName("email"));
		
		CreateTableRequest createTableRequest = rf.resolveCreateTableRequest();
		
		assertEquals("users", createTableRequest.getTableName());
		assertEquals("uid", createTableRequest.getKeySchema().get(0).getAttributeName());
		assertEquals("HASH", createTableRequest.getKeySchema().get(0).getKeyType());
		assertEquals(1, createTableRequest.getKeySchema().size());
		
		assertEquals("uid", createTableRequest.getAttributeDefinitions().get(0).getAttributeName());
		assertEquals("S", createTableRequest.getAttributeDefinitions().get(0).getAttributeType());
		assertEquals("email", createTableRequest.getAttributeDefinitions().get(1).getAttributeName());
		assertEquals("S", createTableRequest.getAttributeDefinitions().get(1).getAttributeType());
		assertEquals(2, createTableRequest.getAttributeDefinitions().size());
		
		assertEquals(1L, createTableRequest.getProvisionedThroughput().getReadCapacityUnits().longValue());
		assertEquals(1L, createTableRequest.getProvisionedThroughput().getWriteCapacityUnits().longValue());
		
		assertFalse(createTableRequest.getSSESpecification().isEnabled());
		assertNull(createTableRequest.getStreamSpecification());
		
		assertEquals("users-email-gsi", createTableRequest.getGlobalSecondaryIndexes().get(0).getIndexName());
		assertEquals("email", createTableRequest.getGlobalSecondaryIndexes().get(0).getKeySchema().get(0).getAttributeName());
		assertEquals(KeyType.HASH.toString(), createTableRequest.getGlobalSecondaryIndexes().get(0).getKeySchema().get(0).getKeyType());
		assertEquals(1, createTableRequest.getGlobalSecondaryIndexes().get(0).getKeySchema().size());
		assertEquals(new Projection().withProjectionType(ProjectionType.ALL), createTableRequest.getGlobalSecondaryIndexes().get(0).getProjection());
		assertEquals(1L, createTableRequest.getGlobalSecondaryIndexes().get(0).getProvisionedThroughput().getReadCapacityUnits().longValue());
		assertEquals(1L, createTableRequest.getGlobalSecondaryIndexes().get(0).getProvisionedThroughput().getWriteCapacityUnits().longValue());
		assertEquals(1, createTableRequest.getGlobalSecondaryIndexes().size());
		
		assertNull(createTableRequest.getLocalSecondaryIndexes());
		
		GetItemSpec getItemSpec = rf.resolveGetItemSpec("123");
		assertEquals("uid", getItemSpec.getKeyComponents().iterator().next().getName());
		assertEquals("123", getItemSpec.getKeyComponents().iterator().next().getValue());
		assertEquals(1, getItemSpec.getKeyComponents().size());
		
		Item resolvedItem = rf.resolveItem(new InfinispanEntry<>(
			"123",
			"alice@wonderland.net",
			null));
		
		assertEquals("123", resolvedItem.getString("uid"));
		assertEquals("alice@wonderland.net", resolvedItem.getString("email"));
		assertEquals(2, resolvedItem.numberOfAttributes());
		
		DeleteItemSpec deleteItemSpec = rf.resolveDeleteItemSpec("123");
		assertEquals("uid", deleteItemSpec.getKeyComponents().iterator().next().getName());
		assertEquals("123", deleteItemSpec.getKeyComponents().iterator().next().getValue());
		assertEquals(1, deleteItemSpec.getKeyComponents().size());
		assertEquals(ReturnValue.ALL_OLD.name(), deleteItemSpec.getReturnValues());
		
		Item item = rf.applyOptionalRangeKey(resolvedItem);
		assertEquals("123", item.getString("uid"));
		assertEquals("alice@wonderland.net", item.getString("email"));
		assertEquals(2, item.numberOfAttributes());
	}
	
	
	public void testWithTablePrefix() {
		
		ItemTransformer tr = new ItemTransformer();
		
		RequestFactory<String,String> rf = new RequestFactory<>(
			tr,
			null,
			new ProvisionedThroughput(1L, 1L),
			false,
			"prefix-",
			null,
			null,
			false);
		
		assertFalse(rf.appliesRangeKey());
		
		assertEquals("prefix-users", rf.getTableName());
		assertEquals("prefix-users", rf.getTableName());
		
		assertNull(rf.getRangeKeyGSIName());
		
		CreateTableRequest createTableRequest = rf.resolveCreateTableRequest();
		assertEquals("prefix-users", createTableRequest.getTableName());
		assertEquals("uid", createTableRequest.getKeySchema().get(0).getAttributeName());
		assertEquals("HASH", createTableRequest.getKeySchema().get(0).getKeyType());
		assertEquals(1, createTableRequest.getKeySchema().size());
		assertEquals("uid", createTableRequest.getAttributeDefinitions().get(0).getAttributeName());
		assertEquals("S", createTableRequest.getAttributeDefinitions().get(0).getAttributeType());
		assertEquals(1, createTableRequest.getAttributeDefinitions().size());
		assertEquals(1L, createTableRequest.getProvisionedThroughput().getReadCapacityUnits().longValue());
		assertEquals(1L, createTableRequest.getProvisionedThroughput().getWriteCapacityUnits().longValue());
		assertNull(createTableRequest.getGlobalSecondaryIndexes());
		assertNull(createTableRequest.getLocalSecondaryIndexes());
		
		GetItemSpec getItemSpec = rf.resolveGetItemSpec("123");
		assertEquals("uid", getItemSpec.getKeyComponents().iterator().next().getName());
		assertEquals("123", getItemSpec.getKeyComponents().iterator().next().getValue());
		assertEquals(1, getItemSpec.getKeyComponents().size());
		
		Item resolvedItem = rf.resolveItem(new InfinispanEntry<>(
			"123",
			"alice@wonderland.net",
			null));
		
		assertEquals("123", resolvedItem.getString("uid"));
		assertEquals("alice@wonderland.net", resolvedItem.getString("email"));
		assertEquals(2, resolvedItem.numberOfAttributes());
		
		DeleteItemSpec deleteItemSpec = rf.resolveDeleteItemSpec("123");
		assertEquals("uid", deleteItemSpec.getKeyComponents().iterator().next().getName());
		assertEquals("123", deleteItemSpec.getKeyComponents().iterator().next().getValue());
		assertEquals(1, deleteItemSpec.getKeyComponents().size());
		assertEquals(ReturnValue.ALL_OLD.name(), deleteItemSpec.getReturnValues());
		
		Item item = rf.applyOptionalRangeKey(resolvedItem);
		assertEquals("123", item.getString("uid"));
		assertEquals("alice@wonderland.net", item.getString("email"));
		assertEquals(2, item.numberOfAttributes());
	}
	
	
	public void testWithRangeKey() {
		
		ItemTransformer tr = new ItemTransformer();
		
		RequestFactory<String,String> rf = new RequestFactory<>(
			tr,
			null,
			new ProvisionedThroughput(10L, 10L),
			false,
			"",
			"tid",
			"789",
			false);
		
		assertTrue(rf.appliesRangeKey());
		
		assertEquals("users", rf.getTableName());
		assertEquals(tr.getTableName(), rf.getTableName());
		
		assertEquals("users-tid-gsi", rf.getRangeKeyGSIName());
		
		CreateTableRequest createTableRequest = rf.resolveCreateTableRequest();
		assertEquals("users", createTableRequest.getTableName());
		assertEquals("uid", createTableRequest.getKeySchema().get(0).getAttributeName());
		assertEquals("HASH", createTableRequest.getKeySchema().get(0).getKeyType());
		assertEquals("tid", createTableRequest.getKeySchema().get(1).getAttributeName());
		assertEquals("RANGE", createTableRequest.getKeySchema().get(1).getKeyType());
		assertEquals(2, createTableRequest.getKeySchema().size());
		assertEquals("uid", createTableRequest.getAttributeDefinitions().get(0).getAttributeName());
		assertEquals("S", createTableRequest.getAttributeDefinitions().get(0).getAttributeType());
		assertEquals("tid", createTableRequest.getAttributeDefinitions().get(1).getAttributeName());
		assertEquals("S", createTableRequest.getAttributeDefinitions().get(1).getAttributeType());
		assertEquals(2, createTableRequest.getAttributeDefinitions().size());
		assertEquals(10L, createTableRequest.getProvisionedThroughput().getReadCapacityUnits().longValue());
		assertEquals(10L, createTableRequest.getProvisionedThroughput().getWriteCapacityUnits().longValue());
		assertEquals("users-tid-gsi", createTableRequest.getGlobalSecondaryIndexes().get(0).getIndexName());
		assertEquals(1, createTableRequest.getGlobalSecondaryIndexes().size());
		assertNull(createTableRequest.getLocalSecondaryIndexes());
		
		GetItemSpec getItemSpec = rf.resolveGetItemSpec("123");
		Iterator<KeyAttribute> it = getItemSpec.getKeyComponents().iterator();
		KeyAttribute hashKeyAttribute = it.next();
		assertEquals("uid", hashKeyAttribute.getName());
		assertEquals("123", hashKeyAttribute.getValue());
		KeyAttribute rangeKeyAttribute = it.next();
		assertEquals("tid", rangeKeyAttribute.getName());
		assertEquals("789", rangeKeyAttribute.getValue());
		assertEquals(2, getItemSpec.getKeyComponents().size());
		
		Item resolvedItem = rf.resolveItem(new InfinispanEntry<>(
			"123",
			"alice@wonderland.net",
			null));
		
		assertEquals("123", resolvedItem.getString("uid"));
		assertEquals("789", resolvedItem.getString("tid"));
		assertEquals("alice@wonderland.net", resolvedItem.getString("email"));
		assertEquals(3, resolvedItem.numberOfAttributes());
		
		DeleteItemSpec deleteItemSpec = rf.resolveDeleteItemSpec("123");
		it = deleteItemSpec.getKeyComponents().iterator();
		hashKeyAttribute = it.next();
		assertEquals("uid", hashKeyAttribute.getName());
		assertEquals("123", hashKeyAttribute.getValue());
		rangeKeyAttribute = it.next();
		assertEquals("tid", rangeKeyAttribute.getName());
		assertEquals("789", rangeKeyAttribute.getValue());
		assertEquals(2, deleteItemSpec.getKeyComponents().size());
		assertEquals(ReturnValue.ALL_OLD.name(), deleteItemSpec.getReturnValues());
		
		Item item = rf.applyOptionalRangeKey(resolvedItem);
		assertEquals("123", item.getString("uid"));
		assertEquals("789", item.getString("tid"));
		assertEquals("alice@wonderland.net", item.getString("email"));
		assertEquals(3, item.numberOfAttributes());
	}
	
	
	public void testWithRangeKeyAndQueryExecutor() {
		
		ItemTransformer tr = new ItemTransformer();
		
		RequestFactory<String,String> rf = new RequestFactory<>(
			tr,
			Collections.singleton("email"),
			new ProvisionedThroughput(10L, 10L),
			false,
			"",
			"tid",
			"789",
			false);
		
		assertTrue(rf.appliesRangeKey());
		
		assertEquals("users", rf.getTableName());
		assertEquals(tr.getTableName(), rf.getTableName());
		
		assertEquals("users-tid-gsi", rf.getRangeKeyGSIName());
		
		CreateTableRequest createTableRequest = rf.resolveCreateTableRequest();
		
		assertEquals("users", createTableRequest.getTableName());
		assertEquals("uid", createTableRequest.getKeySchema().get(0).getAttributeName());
		assertEquals("HASH", createTableRequest.getKeySchema().get(0).getKeyType());
		assertEquals("tid", createTableRequest.getKeySchema().get(1).getAttributeName());
		assertEquals("RANGE", createTableRequest.getKeySchema().get(1).getKeyType());
		assertEquals(2, createTableRequest.getKeySchema().size());
		
		assertEquals("uid", createTableRequest.getAttributeDefinitions().get(0).getAttributeName());
		assertEquals("S", createTableRequest.getAttributeDefinitions().get(0).getAttributeType());
		assertEquals("tid", createTableRequest.getAttributeDefinitions().get(1).getAttributeName());
		assertEquals("S", createTableRequest.getAttributeDefinitions().get(1).getAttributeType());
		assertEquals("email", createTableRequest.getAttributeDefinitions().get(2).getAttributeName());
		assertEquals("S", createTableRequest.getAttributeDefinitions().get(2).getAttributeType());
		assertEquals(3, createTableRequest.getAttributeDefinitions().size());
		
		assertEquals(10L, createTableRequest.getProvisionedThroughput().getReadCapacityUnits().longValue());
		assertEquals(10L, createTableRequest.getProvisionedThroughput().getWriteCapacityUnits().longValue());
		
		assertEquals("users-tid-gsi", createTableRequest.getGlobalSecondaryIndexes().get(0).getIndexName());
		assertEquals("users-email-gsi", createTableRequest.getGlobalSecondaryIndexes().get(1).getIndexName());
		assertEquals(KeyType.HASH.toString(), createTableRequest.getGlobalSecondaryIndexes().get(0).getKeySchema().get(0).getKeyType());
		assertEquals(KeyType.HASH.toString(), createTableRequest.getGlobalSecondaryIndexes().get(1).getKeySchema().get(0).getKeyType());
		assertEquals(1, createTableRequest.getGlobalSecondaryIndexes().get(0).getKeySchema().size());
		assertEquals(1, createTableRequest.getGlobalSecondaryIndexes().get(1).getKeySchema().size());
		assertEquals(new Projection().withProjectionType(ProjectionType.ALL), createTableRequest.getGlobalSecondaryIndexes().get(0).getProjection());
		assertEquals(new Projection().withProjectionType(ProjectionType.ALL), createTableRequest.getGlobalSecondaryIndexes().get(1).getProjection());
		assertEquals(10L, createTableRequest.getGlobalSecondaryIndexes().get(0).getProvisionedThroughput().getReadCapacityUnits().longValue());
		assertEquals(10L, createTableRequest.getGlobalSecondaryIndexes().get(0).getProvisionedThroughput().getWriteCapacityUnits().longValue());
		assertEquals(10L, createTableRequest.getGlobalSecondaryIndexes().get(1).getProvisionedThroughput().getReadCapacityUnits().longValue());
		assertEquals(10L, createTableRequest.getGlobalSecondaryIndexes().get(1).getProvisionedThroughput().getWriteCapacityUnits().longValue());
		assertEquals(2, createTableRequest.getGlobalSecondaryIndexes().size());
		assertNull(createTableRequest.getLocalSecondaryIndexes());
		
		GetItemSpec getItemSpec = rf.resolveGetItemSpec("123");
		Iterator<KeyAttribute> it = getItemSpec.getKeyComponents().iterator();
		KeyAttribute hashKeyAttribute = it.next();
		assertEquals("uid", hashKeyAttribute.getName());
		assertEquals("123", hashKeyAttribute.getValue());
		KeyAttribute rangeKeyAttribute = it.next();
		assertEquals("tid", rangeKeyAttribute.getName());
		assertEquals("789", rangeKeyAttribute.getValue());
		assertEquals(2, getItemSpec.getKeyComponents().size());
		
		Item resolvedItem = rf.resolveItem(new InfinispanEntry<>(
			"123",
			"alice@wonderland.net",
			null));
		
		assertEquals("123", resolvedItem.getString("uid"));
		assertEquals("789", resolvedItem.getString("tid"));
		assertEquals("alice@wonderland.net", resolvedItem.getString("email"));
		assertEquals(3, resolvedItem.numberOfAttributes());
		
		DeleteItemSpec deleteItemSpec = rf.resolveDeleteItemSpec("123");
		it = deleteItemSpec.getKeyComponents().iterator();
		hashKeyAttribute = it.next();
		assertEquals("uid", hashKeyAttribute.getName());
		assertEquals("123", hashKeyAttribute.getValue());
		rangeKeyAttribute = it.next();
		assertEquals("tid", rangeKeyAttribute.getName());
		assertEquals("789", rangeKeyAttribute.getValue());
		assertEquals(2, deleteItemSpec.getKeyComponents().size());
		assertEquals(ReturnValue.ALL_OLD.name(), deleteItemSpec.getReturnValues());
		
		Item item = rf.applyOptionalRangeKey(resolvedItem);
		assertEquals("123", item.getString("uid"));
		assertEquals("789", item.getString("tid"));
		assertEquals("alice@wonderland.net", item.getString("email"));
		assertEquals(3, item.numberOfAttributes());
	}
	
	
	public void testWithTableEncryptionAtRest() {
		
		ItemTransformer tr = new ItemTransformer();
		
		RequestFactory<String,String> rf = new RequestFactory<>(
			tr,
			null,
			new ProvisionedThroughput(1L, 1L),
			true,
			"",
			null,
			null,
			false);
		
		assertFalse(rf.appliesRangeKey());
		
		assertEquals("users", rf.getTableName());
		assertEquals(tr.getTableName(), rf.getTableName());
		
		assertNull(rf.getRangeKeyGSIName());
		
		CreateTableRequest createTableRequest = rf.resolveCreateTableRequest();
		assertEquals("users", createTableRequest.getTableName());
		assertEquals("uid", createTableRequest.getKeySchema().get(0).getAttributeName());
		assertEquals("HASH", createTableRequest.getKeySchema().get(0).getKeyType());
		assertEquals(1, createTableRequest.getKeySchema().size());
		assertEquals("uid", createTableRequest.getAttributeDefinitions().get(0).getAttributeName());
		assertEquals("S", createTableRequest.getAttributeDefinitions().get(0).getAttributeType());
		assertEquals(1, createTableRequest.getAttributeDefinitions().size());
		assertEquals(1L, createTableRequest.getProvisionedThroughput().getReadCapacityUnits().longValue());
		assertEquals(1L, createTableRequest.getProvisionedThroughput().getWriteCapacityUnits().longValue());
		assertTrue(createTableRequest.getSSESpecification().isEnabled());
		assertNull(createTableRequest.getStreamSpecification());
		assertNull(createTableRequest.getGlobalSecondaryIndexes());
		assertNull(createTableRequest.getLocalSecondaryIndexes());
		
		GetItemSpec getItemSpec = rf.resolveGetItemSpec("123");
		assertEquals("uid", getItemSpec.getKeyComponents().iterator().next().getName());
		assertEquals("123", getItemSpec.getKeyComponents().iterator().next().getValue());
		assertEquals(1, getItemSpec.getKeyComponents().size());
		
		Item resolvedItem = rf.resolveItem(new InfinispanEntry<>(
			"123",
			"alice@wonderland.net",
			null));
		
		assertEquals("123", resolvedItem.getString("uid"));
		assertEquals("alice@wonderland.net", resolvedItem.getString("email"));
		assertEquals(2, resolvedItem.numberOfAttributes());
		
		DeleteItemSpec deleteItemSpec = rf.resolveDeleteItemSpec("123");
		assertEquals("uid", deleteItemSpec.getKeyComponents().iterator().next().getName());
		assertEquals("123", deleteItemSpec.getKeyComponents().iterator().next().getValue());
		assertEquals(1, deleteItemSpec.getKeyComponents().size());
		assertEquals(ReturnValue.ALL_OLD.name(), deleteItemSpec.getReturnValues());
		
		Item item = rf.applyOptionalRangeKey(resolvedItem);
		assertEquals("123", item.getString("uid"));
		assertEquals("alice@wonderland.net", item.getString("email"));
		assertEquals(2, item.numberOfAttributes());
	}
	
	
	public void testWithEnableStream() {
		
		ItemTransformer tr = new ItemTransformer();
		
		RequestFactory<String,String> rf = new RequestFactory<>(
			tr,
			null,
			new ProvisionedThroughput(1L, 1L),
			false,
			"",
			null,
			null,
			true);
		
		assertFalse(rf.appliesRangeKey());
		
		assertEquals("users", rf.getTableName());
		assertEquals(tr.getTableName(), rf.getTableName());
		
		assertNull(rf.getRangeKeyGSIName());
		
		CreateTableRequest createTableRequest = rf.resolveCreateTableRequest();
		assertEquals("users", createTableRequest.getTableName());
		assertEquals("uid", createTableRequest.getKeySchema().get(0).getAttributeName());
		assertEquals("HASH", createTableRequest.getKeySchema().get(0).getKeyType());
		assertEquals(1, createTableRequest.getKeySchema().size());
		assertEquals("uid", createTableRequest.getAttributeDefinitions().get(0).getAttributeName());
		assertEquals("S", createTableRequest.getAttributeDefinitions().get(0).getAttributeType());
		assertEquals(1, createTableRequest.getAttributeDefinitions().size());
		assertEquals(1L, createTableRequest.getProvisionedThroughput().getReadCapacityUnits().longValue());
		assertEquals(1L, createTableRequest.getProvisionedThroughput().getWriteCapacityUnits().longValue());
		assertFalse(createTableRequest.getSSESpecification().isEnabled());
		assertTrue(createTableRequest.getStreamSpecification().isStreamEnabled());
		assertEquals(StreamViewType.NEW_AND_OLD_IMAGES.name(), createTableRequest.getStreamSpecification().getStreamViewType());
		assertNull(createTableRequest.getGlobalSecondaryIndexes());
		assertNull(createTableRequest.getLocalSecondaryIndexes());
		
		GetItemSpec getItemSpec = rf.resolveGetItemSpec("123");
		assertEquals("uid", getItemSpec.getKeyComponents().iterator().next().getName());
		assertEquals("123", getItemSpec.getKeyComponents().iterator().next().getValue());
		assertEquals(1, getItemSpec.getKeyComponents().size());
		
		Item resolvedItem = rf.resolveItem(new InfinispanEntry<>(
			"123",
			"alice@wonderland.net",
			null));
		
		assertEquals("123", resolvedItem.getString("uid"));
		assertEquals("alice@wonderland.net", resolvedItem.getString("email"));
		assertEquals(2, resolvedItem.numberOfAttributes());
		
		DeleteItemSpec deleteItemSpec = rf.resolveDeleteItemSpec("123");
		assertEquals("uid", deleteItemSpec.getKeyComponents().iterator().next().getName());
		assertEquals("123", deleteItemSpec.getKeyComponents().iterator().next().getValue());
		assertEquals(1, deleteItemSpec.getKeyComponents().size());
		assertEquals(ReturnValue.ALL_OLD.name(), deleteItemSpec.getReturnValues());
		
		Item item = rf.applyOptionalRangeKey(resolvedItem);
		assertEquals("123", item.getString("uid"));
		assertEquals("alice@wonderland.net", item.getString("email"));
		assertEquals(2, item.numberOfAttributes());
	}
	
	
	public void testSanitizeEmptyEmail() {
		
		ItemTransformer tr = new ItemTransformer();
		
		RequestFactory<String,String> rf = new RequestFactory<>(
			tr,
			null,
			new ProvisionedThroughput(1L, 1L),
			false,
			"",
			null,
			null,
			false);
		
		Item resolvedItem = rf.resolveItem(new InfinispanEntry<>(
			"123",
			"",
			null));
		
		assertEquals("123", resolvedItem.getString("uid"));
		assertEquals(1, resolvedItem.numberOfAttributes());
	}
}
