package com.nimbusds.infinispan.persistence.dynamodb.logging;


import com.nimbusds.infinispan.persistence.dynamodb.logging.Loggers;
import junit.framework.TestCase;


public class LoggersTest extends TestCase{
	

	public void testNames() {

		assertEquals("MAIN", Loggers.MAIN_LOG.getName());
		assertEquals("DYN", Loggers.DYNAMODB_LOG.getName());
	}
}
