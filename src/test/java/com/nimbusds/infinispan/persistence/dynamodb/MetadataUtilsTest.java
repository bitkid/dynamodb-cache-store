package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import junit.framework.TestCase;
import org.infinispan.metadata.InternalMetadata;


public class MetadataUtilsTest extends TestCase {
	
	
	public void testAddAndParse() {
		
		Item item = new Item();
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(100L)
			.lifespan(200L)
			.maxIdle(300L)
			.lastUsed(400L)
			.build();
		
		item = MetadataUtils.addMetadata(item, metadata);
		
		assertEquals(100L, item.getLong("iat"));
		assertEquals(200L, item.getLong("max"));
		assertEquals(300L, item.getLong("idl"));
		assertEquals(400L, item.getLong("lat"));
		assertEquals(4, item.numberOfAttributes());
		
		metadata = MetadataUtils.parseMetadata(item);
		
		assertEquals(100L, metadata.created());
		assertEquals(200L, metadata.lifespan());
		assertEquals(300L, metadata.maxIdle());
		assertEquals(400L, metadata.lastUsed());
	}
	
	
	public void testAddAndParse_allZero() {
		
		Item item = new Item();
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(0L)
			.lifespan(0L)
			.maxIdle(0L)
			.lastUsed(0L)
			.build();
		
		item = MetadataUtils.addMetadata(item, metadata);
		
		assertEquals(0L, item.getLong("iat"));
		assertEquals(0L, item.getLong("max"));
		assertEquals(0L, item.getLong("idl"));
		assertEquals(0L, item.getLong("lat"));
		assertEquals(4, item.numberOfAttributes());
		
		metadata = MetadataUtils.parseMetadata(item);
		
		assertEquals(0L, metadata.created());
		assertEquals(0L, metadata.lifespan());
		assertEquals(0L, metadata.maxIdle());
		assertEquals(0L, metadata.lastUsed());
	}
	
	
	public void testAddAndParse_allNegative() {
		
		Item item = new Item();
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(-1L)
			.lifespan(-1L)
			.maxIdle(-1L)
			.lastUsed(-1L)
			.build();
		
		item = MetadataUtils.addMetadata(item, metadata);
		
		assertEquals(0, item.numberOfAttributes());
		
		metadata = MetadataUtils.parseMetadata(item);
		
		assertEquals(-1L, metadata.created());
		assertEquals(-1L, metadata.lifespan());
		assertEquals(-1L, metadata.maxIdle());
		assertEquals(-1L, metadata.lastUsed());
	}
}
