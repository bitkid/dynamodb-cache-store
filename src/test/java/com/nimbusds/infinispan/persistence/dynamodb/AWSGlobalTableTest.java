package com.nimbusds.infinispan.persistence.dynamodb;


import java.util.Collection;
import java.util.LinkedList;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;
import org.junit.After;
import org.junit.Test;


/**
 * Tests a global table lifecycle.
 */
public class AWSGlobalTableTest {
	
	
	private static final String EU_CENTRAL_1 = "eu-central-1";
	
	private static final String US_EAST_1 = "us-east-1";
	
	private static final String TABLE_NAME = "my_global_table";
	
	
	@After
	public void cleanUp() {
		
		AmazonDynamoDB client1 = AmazonDynamoDBClientBuilder
			.standard()
			.withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
			.withRegion(EU_CENTRAL_1)
			.build();
		
		AmazonDynamoDB client2 = AmazonDynamoDBClientBuilder
			.standard()
			.withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
			.withRegion(US_EAST_1)
			.build();
		
		client1.deleteTable(TABLE_NAME);
		client2.deleteTable(TABLE_NAME);
	}
	
	
//	@Test
	public void createGlobalTable() throws InterruptedException {
		
		// Create a client for each of the regions with table replicas
		AmazonDynamoDB client1 = AmazonDynamoDBClientBuilder
			.standard()
			.withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
			.withRegion(EU_CENTRAL_1)
			.build();
		
		AmazonDynamoDB client2 = AmazonDynamoDBClientBuilder
			.standard()
			.withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
			.withRegion(US_EAST_1)
			.build();
		
		ListTablesResult listTablesResult = client1.listTables();
		System.out.println(listTablesResult);
		
		// The table definition, must be enabled for streaming
		Collection<KeySchemaElement> keyAttrs = new LinkedList<>();
		keyAttrs.add(new KeySchemaElement("id", KeyType.HASH));
		
		Collection<AttributeDefinition> attrs = new LinkedList<>();
		attrs.add(new AttributeDefinition("id", ScalarAttributeType.S));
		
		CreateTableRequest createTableRequest = new CreateTableRequest()
			.withTableName(TABLE_NAME)
			.withKeySchema(keyAttrs)
			.withAttributeDefinitions(attrs)
			.withStreamSpecification(
				new StreamSpecification()
					.withStreamEnabled(true)
					.withStreamViewType(StreamViewType.NEW_AND_OLD_IMAGES))
			.withProvisionedThroughput(new ProvisionedThroughput(1L, 1L));
		
		// Create the table in each region
		new DynamoDB(client1).createTable(createTableRequest).waitForActive();
		new DynamoDB(client2).createTable(createTableRequest).waitForActive();
		
		// The global table request can be executed in any of the regions
		CreateGlobalTableResult createGlobalTableResult = client1.createGlobalTable(
			new CreateGlobalTableRequest()
				.withGlobalTableName(TABLE_NAME)
				.withReplicationGroup(
					new Replica().withRegionName(EU_CENTRAL_1),
					new Replica().withRegionName(US_EAST_1)
				));
		
		System.out.println(createGlobalTableResult.getGlobalTableDescription());
	}
}
