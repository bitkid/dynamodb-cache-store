package com.nimbusds.infinispan.persistence.dynamodb;


import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;


public class DynamoDBStoreWithXMLConfig_continuousBackupsTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";
	
	
	protected EmbeddedCacheManager cacheMgr;
	
	
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();

		setUpLocalClient();

		cacheMgr = new DefaultCacheManager("test-infinispan-continuous-backups.xml");

		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	
	
	public void testWithContinuousBackups() {
		
		assertTrue(((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).isEnableContinuousBackups());
		
		// Will trigger init
		// assertEquals(0, cacheMgr.getCache(CACHE_NAME).size());
	}
}