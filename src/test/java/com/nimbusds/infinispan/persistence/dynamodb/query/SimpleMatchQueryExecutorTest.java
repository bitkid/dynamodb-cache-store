package com.nimbusds.infinispan.persistence.dynamodb.query;


import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.nimbusds.infinispan.persistence.common.query.MatchQuery;
import com.nimbusds.infinispan.persistence.common.query.MultiMatchQuery;
import com.nimbusds.infinispan.persistence.common.query.SimpleMatchQuery;
import junit.framework.TestCase;


public class SimpleMatchQueryExecutorTest extends TestCase {
	
	
	public void testConvertToSimpleMatchQuery_cast() {
		
		MatchQuery<String, String> query = new SimpleMatchQuery<>("key", "value");
		
		assertEquals(query, SimpleMatchQueryExecutor.toSimpleMatchQuery(query));
	}
	
	
	public void testConvertToSimpleMatchQuery_fromMultiMatchQuery() {
		
		Map<String, String> map = new HashMap<>();
		map.put("key", "value");
		MultiMatchQuery<String, String> multiMatchQuery = new MultiMatchQuery<>(map);
		
		SimpleMatchQuery<String, String> simpleMatchQuery = SimpleMatchQueryExecutor.toSimpleMatchQuery(multiMatchQuery);
		
		assertEquals("key", simpleMatchQuery.getKey());
		assertEquals("value", simpleMatchQuery.getValue());
	}
	
	
	public void testToQuerySpec() {
		
		SimpleMatchQuery<String, String> query = new SimpleMatchQuery<>("key-1", "value-1");
		QuerySpec querySpec = SimpleMatchQueryExecutor.toQuerySpec(query);
		assertEquals("#k = :value", querySpec.getKeyConditionExpression());
	}
	
	
	public void testToScanSpec() {
		
		SimpleMatchQuery<String, String> query = new SimpleMatchQuery<>("key-1", "value-1");
		ScanSpec scanSpec = SimpleMatchQueryExecutor.toScanSpec(query);
		assertEquals("#k = :value", scanSpec.getFilterExpression());
		assertEquals("key-1", scanSpec.getNameMap().get("#k"));
		assertEquals(1, scanSpec.getNameMap().size());
		assertEquals("value-1", scanSpec.getValueMap().get(":value"));
		assertEquals(1, scanSpec.getValueMap().size());
		assertFalse(scanSpec.isConsistentRead());
	}
}
