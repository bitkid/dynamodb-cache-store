package com.nimbusds.infinispan.persistence.dynamodb;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;


/**
 * The base class for tests that require a DynamoDB database.
 */
public class TestWithDynamoDB extends TestCase {
	
	
	public static final String ENDPOINT;
	
	
	public static final String AWS_ACCESS_KEY_ID;
	
	
	public static final String AWS_SECRET_ACCESS_KEY;
	
	
	static {
		try {
			Properties testProps = new Properties();
			testProps.load(new FileInputStream("test.properties"));
			
			System.getProperties().putAll(testProps);
			
			ENDPOINT = testProps.getProperty("dynamoDB.endpoint");
			AWS_ACCESS_KEY_ID = testProps.getProperty("aws.accessKeyId");
			AWS_SECRET_ACCESS_KEY = testProps.getProperty("aws.secretKey");
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	protected AmazonDynamoDB client;
	
	
	protected DynamoDB dynamoDB;
	
	
	protected void setUpLocalClient() {
		
		client = AmazonDynamoDBClientBuilder
			.standard()
			.withEndpointConfiguration(
				new AwsClientBuilder.EndpointConfiguration(ENDPOINT, null))
			.withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
			.build();
		
		dynamoDB = new DynamoDB(client);
	}
	
	
	@Before
	@Override
	public void setUp()
		throws Exception {
		
		setUpLocalClient();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (dynamoDB != null) {
			
			List<String> tableNames = client.listTables().getTableNames();
			
			for (String tableName: tableNames) {
				
				if (! tableName.contains("test")) {
					return;
				}
				
				Table table = dynamoDB.getTable(tableName);
				table.delete();
				table.waitForDelete();
			}
			
			dynamoDB.shutdown();
		}
	}
	
	
	public void testEnvironment() {
		
		System.out.println("ENDPOINT: " + ENDPOINT);
		
		assertNotNull(ENDPOINT);
		assertNotNull(AWS_ACCESS_KEY_ID);
		assertNotNull(AWS_SECRET_ACCESS_KEY);
	}
}
