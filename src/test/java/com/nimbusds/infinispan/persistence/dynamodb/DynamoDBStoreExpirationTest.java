package com.nimbusds.infinispan.persistence.dynamodb;


import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashSet;

import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfigurationBuilder;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.context.Flag;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Before;


public class DynamoDBStoreExpirationTest extends TestWithDynamoDB {
	
	
	
	public static final String CACHE_NAME = "users";
	
	
	protected EmbeddedCacheManager cacheMgr;
	
	
	@Before
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		cacheMgr = new DefaultCacheManager();
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(DynamoDBStoreConfigurationBuilder.class)
			.endpoint(ENDPOINT)
			.itemTransformerClass(ExpiringUserItemTransformer.class)
			.create();
		
		b.memory()
			.evictionStrategy(EvictionStrategy.REMOVE)
			.size(10)
			.evictionType(EvictionType.COUNT)
			.create();
		
		cacheMgr.defineConfiguration(CACHE_NAME, b.build());
		
		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	
	
	public void testExpiration()
		throws Exception {
		
		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertEquals(10, myMap.getCacheConfiguration().memory().size());
		assertEquals(EvictionType.COUNT, myMap.getCacheConfiguration().memory().evictionType());
		assertEquals(60000L, myMap.getCacheConfiguration().expiration().wakeUpInterval());
		
		String[] keys = new String[100];
		
		Instant created = Instant.now().minus(1L, ChronoUnit.DAYS);
		
		for (int i=0; i < 100; i ++) {
			
			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com", created, new HashSet<>(Arrays.asList("admin", "audit")));
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		assertEquals(10, myMap.getAdvancedCache().withFlags(Flag.SKIP_CACHE_LOAD).size());
		assertEquals(100, myMap.size());
	
		// reap expired entries
		myMap.getAdvancedCache().getExpirationManager().processExpiration();
		
		while (10 != myMap.size()) {
			Thread.sleep(100);
		}
		
		assertEquals(10, myMap.size()); // only in-memory entries remain
	}
}
