package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.model.*;


public class DynamoDBDeleteItemTest extends TestWithDynamoDB {
	
	
	public void testDeleteOutcome_noReturnValues()
		throws Exception {
		
		Table table = dynamoDB.createTable(new CreateTableRequest()
			.withTableName("test-deletion")
			.withKeySchema(new KeySchemaElement("key", KeyType.HASH))
			.withAttributeDefinitions(new AttributeDefinition("key", ScalarAttributeType.S))
			.withProvisionedThroughput(new ProvisionedThroughput(1L, 1L)));
		
		table.waitForActive();
		
		table.putItem(new Item()
			.withPrimaryKey("key", "1")
			.with("apples", 10)
			.with("pears", 20));
		
		DeleteItemOutcome dio = table.deleteItem(new DeleteItemSpec().withPrimaryKey("key", "1"));
		
		assertNull(dio.getItem());
		assertNotNull(dio.getDeleteItemResult());
		assertNull(dio.getDeleteItemResult().getAttributes());
		
		dio = table.deleteItem(new DeleteItemSpec().withPrimaryKey("key", "invalid-key"));
		
		assertNull(dio.getItem());
		assertNotNull(dio.getDeleteItemResult());
		assertNull(dio.getDeleteItemResult().getAttributes());
	}
	
	
	public void testDeleteOutcome_withReturnValues()
		throws Exception {
		
		Table table = dynamoDB.createTable(new CreateTableRequest()
			.withTableName("test-deletion")
			.withKeySchema(new KeySchemaElement("key", KeyType.HASH))
			.withAttributeDefinitions(new AttributeDefinition("key", ScalarAttributeType.S))
			.withProvisionedThroughput(new ProvisionedThroughput(1L, 1L)));
		
		table.waitForActive();
		
		table.putItem(new Item()
			.withPrimaryKey("key", "1")
			.with("apples", 10)
			.with("pears", 20));
		
		DeleteItemOutcome dio = table.deleteItem(new DeleteItemSpec().withPrimaryKey("key", "1").withReturnValues(ReturnValue.ALL_OLD));
		
		assertNotNull(dio.getItem());
		assertNotNull(dio.getDeleteItemResult());
		assertNotNull(dio.getDeleteItemResult().getAttributes());
		
		dio = table.deleteItem(new DeleteItemSpec().withPrimaryKey("key", "invalid-key").withReturnValues(ReturnValue.ALL_OLD));
		
		assertNull(dio.getItem());
		assertNotNull(dio.getDeleteItemResult());
		assertNull(dio.getDeleteItemResult().getAttributes());
	}
}
