package com.nimbusds.infinispan.persistence.dynamodb;


import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.Item;


/**
 * Utility for DynamoDB item sanitisation to prevent
 * {@code ValidationException}s.
 *
 * <blockquote>Attribute values cannot be null. String and Binary type
 * attributes must have lengths greater than zero. Set type attributes cannot
 * be empty. Requests with empty values will be rejected with a
 * ValidationException exception.
 * </blockquote>
 */
class ItemSanitization {
	
	
	/**
	 * Sanitises the specified item.
	 *
	 * @param item The item. Must not be {@code null}.
	 *
	 * @return The sanitised item.
	 */
	static Item sanitize(final Item item) {
		
		return Item.fromMap(sanitize(item.asMap()));
	}
	
	
	/**
	 * Sanitises the specified map.
	 *
	 * @param map The map. Must not be {@code null}.
	 *
	 * @return The sanitised map.
	 */
	static Map<String,Object> sanitize(final Map<String,Object> map) {
		
		List<String> keysToRemove = new LinkedList<>();
		
		for (final String key: map.keySet()) {
			
			Object value = map.get(key);
			
			if (value == null) {
				continue; // skip
			}
			
			// Remove empty strings and collections (sets)
			final boolean toRemove = String.class.isAssignableFrom(value.getClass()) && ((String)value).isEmpty()
				|| byte[].class.isAssignableFrom(value.getClass()) && ((byte[])value).length == 0
				|| ByteBuffer.class.isAssignableFrom(value.getClass()) && ((ByteBuffer)value).array().length == 0
				|| Collection.class.isAssignableFrom(value.getClass()) && ((Collection)value).isEmpty();
			
			if (toRemove) {
				keysToRemove.add(key);
			}
			
			if (Map.class.isAssignableFrom(value.getClass())) {
				// Descent into map
				sanitize((Map<String,Object>)value);
			}
		}
		
		keysToRemove.forEach(map::remove);
		
		return map;
	}
	
	
	/**
	 * Prevents public instantiation.
	 */
	private ItemSanitization() {}
}
