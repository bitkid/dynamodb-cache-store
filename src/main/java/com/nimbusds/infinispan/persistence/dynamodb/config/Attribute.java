package com.nimbusds.infinispan.persistence.dynamodb.config;


import java.util.HashMap;
import java.util.Map;


/**
 * DynamoDB store XML configuration attributes.
 */
public enum Attribute {
	
	
	/**
	 * Unknown XML attribute.
	 */
	UNKNOWN(null), // must be first
	
	
	/**
	 * The XML attribute for the DynamoDB endpoint. If set region is
	 * overridden.
	 */
	ENDPOINT("endpoint"),
	
	
	/**
	 * The XML attribute for the DynamoDB region.
	 */
	REGION("region"),
	
	
	/**
	 * The XML attribute for the DynamoDB item transformer.
	 */
	ITEM_TRANSFORMER("item-transformer"),
	
	
	/**
	 * The XML attribute for the DynamoDB query executor.
	 */
	QUERY_EXECUTOR("query-executor"),
	
	
	/**
	 * The XML attribute for the indexed DynamoDB table attributes.
	 */
	INDEXED_ATTRIBUTES("indexed-attributes"),
	
	
	/**
	 * The XML attribute for the read capacity to provision when creating
	 * a new DynamoDB table and indices.
	 */
	READ_CAPACITY("read-capacity"),
	
	
	/**
	 * The XML attribute for the write capacity to provision when creating
	 * a new DynamoDB table and indices.
	 */
	WRITE_CAPACITY("write-capacity"),
	
	
	/**
	 * The XML attribute for creating the DynamoDB table with encryption at
	 * rest.
	 */
	ENCRYPTION_AT_REST("encryption-at-rest"),
	
	
	/**
	 * The XML attribute for the optional DynamoDB table prefix to use.
	 */
	TABLE_PREFIX("table-prefix"),
	
	
	/**
	 * The XML attribute for the optional range key to apply to all
	 * DynamoDB operations.
	 */
	APPLY_RANGE_KEY("apply-range-key"),
	
	
	/**
	 * The XML attribute for the optional value of the range key.
	 */
	RANGE_KEY_VALUE("range-key-value"),
	
	
	/**
	 * The XML attribute to enable a stream for a global table, defaults to
	 * {@code false}.
	 */
	ENABLE_STREAM("enable-stream"),
	
	
	/**
	 * The XML attribute to enable continuous backups / point in time
	 * recovery, defaults to {@code false}.
	 */
	ENABLE_CONTINUOUS_BACKUPS("enable-continuous-backups");
	
	
	/**
	 * The attribute name.
	 */
	private final String name;
	
	
	/**
	 * Creates a new attribute with the specified name.
	 *
	 * @param name The attribute name.
	 */
	Attribute(final String name) {
		this.name = name;
	}
	
	
	/**
	 * Gets the local name of this attribute.
	 *
	 * @return The local name.
	 */
	public String getLocalName() {
		return name;
	}
	
	
	/**
	 * The enumerated attributes as map.
	 */
	private static final Map<String, Attribute> attributes;
	
	
	static {
		final Map<String, Attribute> map = new HashMap<>();
		for (Attribute attribute : values()) {
			final String name = attribute.getLocalName();
			if (name != null) {
				map.put(name, attribute);
			}
		}
		attributes = map;
	}
	
	
	/**
	 * Returns the matching attribute for the specified local name.
	 *
	 * @param localName The local name.
	 *
	 * @return The attribute.
	 */
	public static Attribute forName(final String localName) {
		final Attribute attribute = attributes.get(localName);
		return attribute == null ? UNKNOWN : attribute;
	}
}
