package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;


/**
 * Interface for transforming between Infinispan entries (key / value pair and
 * metadata) and a corresponding DynamoDB item. Implementations must be
 * thread-safe.
 *
 * <p>To specify an entry transformer for a given Infinispan cache that is
 * backed by a DynamoDB store, provide its class name to the
 * {@link DynamoDBStoreConfiguration store configuration}.
 */
public interface DynamoDBItemTransformer<K,V> {
	
	
	/**
	 * Returns the base name of the DynamoDB table. Required to create and
	 * to connect to the DynamoDB table for storing the cache entries. The
	 * final DynamoDB table name is formed by prefixing the optional
	 * configuration {@code table-prefix} to the base name.
	 *
	 * @return The table name.
	 */
	String getTableName();
	
	
	/**
	 * Returns the DynamoDB hash key attribute name. Required to create the
	 * DynamoDB table for storing the cache entries.
	 *
	 * @return The hash key attribute name.
	 */
	String getHashKeyAttributeName();
	
	
	/**
	 * Resolves the DynamoDB hash key (of scalar attribute type string) for
	 * the specified Infinispan entry key.
	 *
	 * @param key The Infinispan entry key. Not {@code null}.
	 *
	 * @return The DynamoDB hash key.
	 */
	String resolveHashKey(final K key);
	

	/**
	 * Transforms the specified Infinispan entry (key / value pair with
	 * optional metadata) to a DynamoDB item.
	 *
	 * <p>Example:
	 *
	 * <p>Infinispan entry:
	 *
	 * <ul>
	 *     <li>Key: cae7t
	 *     <li>Value: Java POJO with fields {@code uid=cae7t},
	 *         {@code givenName=Alice}, {@code surname=Adams} and
	 *         {@code email=alice@wonderland.net}.
	 *     <li>Metadata: Specifies the entry expiration and other
	 *         properties.
	 * </ul>
	 *
	 * <p>Resulting DynamoDB item:
	 *
	 * <pre>
	 * uid: cae7t (key)
	 * surname: Adams
	 * given_name: Alice
	 * email: alice@wonderland.net
	 * </pre>
	 *
	 * @param infinispanEntry The Infinispan entry. Not {@code null}.
	 *
	 * @return The DynamoDB item.
	 */
	Item toItem(final InfinispanEntry<K,V> infinispanEntry);


	/**
	 * Transforms the specified DynamoDB item to an Infinispan entry (key /
	 * value / metadata triple).
	 *
	 * <p>Example:
	 *
	 * <p>DynamoDB item:
	 *
	 * <pre>
	 * uid: cae7t
	 * surname: Adams
	 * given_name: Alice
	 * email: alice@wonderland.net
	 * </pre>
	 *
	 * <p>Resulting Infinispan entry:
	 *
	 * <ul>
	 *     <li>Key: cae7t
	 *     <li>Value: Java POJO with fields {@code uid=cae7t},
	 *         {@code givenName=Alice}, {@code surname=Adams} and
	 *         {@code email=alice@wonderland.net}.
	 *     <li>Metadata: Default metadata (no expiration, etc).
	 * </ul>
	 *
	 * @param item The DynamoDB item. Must not be {@code null}.
	 *
	 * @return The Infinispan entry (key / value pair with optional
	 *         metadata).
	 */
	InfinispanEntry<K,V> toInfinispanEntry(final Item item);
}
