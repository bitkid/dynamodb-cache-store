package com.nimbusds.infinispan.persistence.dynamodb;


import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.nimbusds.common.monitor.MonitorRegistries;
import net.jcip.annotations.ThreadSafe;


/**
 * DynamoDB operations timers.
 */
@ThreadSafe
class DynamoDBTimers {


	/**
	 * Times DynamoDB get operations.
	 */
	final Timer getTimer = new Timer();


	/**
	 * Times DynamoDB put operations.
	 */
	final Timer putTimer = new Timer();


	/**
	 * Times DynamoDB delete operations.
	 */
	final Timer deleteTimer = new Timer();


	/**
	 * Times DynamoDB item process operations.
	 */
	final Timer processTimer = new Timer();


	/**
	 * Times DynamoDB purge expired Infinispan entries operations.
	 */
	final Timer purgeTimer = new Timer();


	/**
	 * Creates a new set of DynamoDB operations timers and puts them into
	 * the singleton {@link MonitorRegistries}.
	 *
	 * @param prefix The prefix for the timer names. Must not be
	 *               {@code null}.
	 */
	DynamoDBTimers(final String prefix) {
		MonitorRegistries.register(prefix + "dynamoDB.getTimer", getTimer);
		MonitorRegistries.register(prefix + "dynamoDB.putTimer", putTimer);
		MonitorRegistries.register(prefix + "dynamoDB.deleteTimer", deleteTimer);
		MonitorRegistries.register(prefix + "dynamoDB.processTimer", processTimer);
		MonitorRegistries.register(prefix + "dynamoDB.purgeTimer", purgeTimer);
	}
	
	
	/**
	 * Creates a new set of DynamoDB operations timers and puts them into
	 * the specified registry.
	 *
	 * @param prefix         The prefix for the timer names. Must not be
	 *                       {@code null}.
	 * @param metricRegistry The metrics registry. Must not be
	 *                       {@code null}.
	 */
	DynamoDBTimers(final String prefix, final MetricRegistry metricRegistry) {
		metricRegistry.register(prefix + "dynamoDB.getTimer", getTimer);
		metricRegistry.register(prefix + "dynamoDB.putTimer", putTimer);
		metricRegistry.register(prefix + "dynamoDB.deleteTimer", deleteTimer);
		metricRegistry.register(prefix + "dynamoDB.processTimer", processTimer);
		metricRegistry.register(prefix + "dynamoDB.purgeTimer", purgeTimer);
	}
}
