package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import org.infinispan.metadata.InternalMetadata;


/**
 * Infinispan metadata persistence utilities.
 */
public class MetadataUtils {
	
	
	/**
	 * Adds Infinispan metadata to the specified DynamoDB item.
	 *
	 * <p>Each metadata timestamp, if set (greater than -1) is encoded as
	 * long integer representing the number of milliseconds since the Unix
	 * spec:
	 *
	 * <ul>
	 *     <li>created - "iat"
	 *     <li>lifespan - "max"
	 *     <li>max idle - "idl"
	 *     <li>last used - "lat"
	 * </ul>
	 *
	 * @param item     The DynamoDB item.
	 * @param metadata The Infinispan metadata, {@code null} if not
	 *                 specified.
	 *
	 * @return The DynamoDB item.
	 */
	public static Item addMetadata(final Item item, final InternalMetadata metadata) {
		
		if (metadata == null) {
			return item;
		}
		
		if (metadata.created() > -1) {
			item.withLong("iat", metadata.created());
		}
		
		if (metadata.lifespan() > -1) {
			item.withLong("max", metadata.lifespan());
		}
		
		if (metadata.maxIdle() > -1) {
			item.withLong("idl", metadata.maxIdle());
		}
		
		if (metadata.lastUsed() > -1) {
			item.withLong("lat", metadata.lastUsed());
		}
		
		return item;
	}
	
	
	/**
	 * Parses Infinispan metadata encoded with {@link #addMetadata} from
	 * the specified DynamoDB item.
	 *
	 * @param item The dynamoDB item.
	 *
	 * @return The Infinispan metadata.
	 */
	public static InternalMetadata parseMetadata(final Item item) {
		
		InternalMetadataBuilder b = new InternalMetadataBuilder();
		
		if (item.hasAttribute("iat")) {
			b.created(item.getLong("iat"));
		}
		
		if (item.hasAttribute("max")) {
			b.lifespan(item.getLong("max"));
		}
		
		if (item.hasAttribute("idl")) {
			b.maxIdle(item.getLong("idl"));
		}
		
		if (item.hasAttribute("lat")) {
			b.lastUsed(item.getLong("lat"));
		}
		
		return b.build();
	}
}
