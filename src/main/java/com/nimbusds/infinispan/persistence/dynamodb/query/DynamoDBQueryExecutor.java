package com.nimbusds.infinispan.persistence.dynamodb.query;


import com.nimbusds.infinispan.persistence.common.query.QueryExecutor;


/**
 * DynamoDB query executor.
 */
public interface DynamoDBQueryExecutor<K, V> extends QueryExecutor<K, V> {
	
	
	/**
	 * Initialises the DynamoDB query executor.
	 *
	 * @param initCtx The initialisation context. Not {@code null}.
	 */
	void init(final DynamoDBQueryExecutorInitContext<K, V> initCtx);
}
