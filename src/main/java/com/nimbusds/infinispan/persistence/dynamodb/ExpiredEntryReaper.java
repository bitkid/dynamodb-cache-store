package com.nimbusds.infinispan.persistence.dynamodb;


import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.amazonaws.services.dynamodbv2.document.Table;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.dynamodb.logging.Loggers;
import net.jcip.annotations.ThreadSafe;
import org.infinispan.marshall.core.MarshalledEntry;
import org.infinispan.marshall.core.MarshalledEntryFactory;
import org.infinispan.metadata.InternalMetadata;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.AdvancedCacheWriter;


/**
 * Reaper of expired persisted Infinispan entries.
 */
@ThreadSafe
class ExpiredEntryReaper<K, V> {
	
	
	/**
	 * The Infinispan marshalled entry factory.
	 */
	private final MarshalledEntryFactory<K, V> marshalledEntryFactory;
	
	
	/**
	 * The DynamoDB table.
	 */
	private final Table table;


	/**
	 * The DynamoDB request factory.
	 */
	private final RequestFactory<K, V> requestFactory;


	/**
	 * Creates a new reaper for expired persisted Infinispan entries.
	 *
	 * @param marshalledEntryFactory The Infinispan marshalled entry.
	 * @param table                  The DynamoDB table. Must not be
	 *                               {@code null}.
	 *                               factory. Must not be {@code null}.
	 * @param requestFactory         The DynamoDB request factory.
	 */
	ExpiredEntryReaper(final MarshalledEntryFactory<K,V> marshalledEntryFactory,
			   final Table table,
			   final RequestFactory<K, V> requestFactory) {
		
		assert marshalledEntryFactory != null;
		this.marshalledEntryFactory = marshalledEntryFactory;
		
		assert table != null;
		this.table = table;
		
		assert requestFactory != null;
		this.requestFactory = requestFactory;
	}


	/**
	 * Purges the expired persisted entries according to their metadata
	 * timestamps (if set / persisted).
	 *
	 * @param purgeListener The purge listener. Must not be {@code null}.
	 *
	 * @return The number of purged entries.
	 */
	int purge(final AdvancedCacheWriter.PurgeListener<? super K> purgeListener) {

		final long now = new Date().getTime();

		// The keys for deletion
		List<K> forDeletion = new LinkedList<>();
		
		requestFactory.getAllItems(table).forEach(item -> {
			
			InfinispanEntry<K, V> infinispanEntry = requestFactory.getItemTransformer().toInfinispanEntry(item);
			
			InternalMetadata metadata = infinispanEntry.getMetadata();
			
			if (metadata == null) {
				return; // no metadata found
			}
			
			if (metadata.isExpired(now)) {
				// Mark item for deletion
				forDeletion.add(infinispanEntry.getKey());
			}
		});

		final AtomicInteger deleteCounter = new AtomicInteger(0);
		
		// Delete batching will not work here, doesn't support
		// attribute retrieval to confirm deletion

		for (K key: forDeletion) {

			final boolean deleted = table.deleteItem(requestFactory.resolveDeleteItemSpec(key)).getItem() != null;
			
			if (deleted) {
				// Notify listener, interested in the Infinispan entry key
				purgeListener.entryPurged(key);
				deleteCounter.incrementAndGet();
			}
		}
		
		Loggers.DYNAMODB_LOG.debug("[DS0128] DynamoDB store: Purged {} expired {} cache entries", deleteCounter.get(), table.getTableName());

		return deleteCounter.get();
	}


	/**
	 * Purges the expired persisted entries according to their metadata
	 * timestamps (if set / persisted), with an extended listener for the
	 * complete purged entry.
	 *
	 * @param purgeListener The purge listener. Must not be {@code null}.
	 *
	 * @return The number of purged entries.
	 */
	int purgeExtended(final AdvancedCacheExpirationWriter.ExpirationPurgeListener<K,V> purgeListener) {

		final long now = new Date().getTime();
		
		// The entries for deletion
		List<InfinispanEntry<K,V>> forDeletion = new LinkedList<>();
		
		requestFactory.getAllItems(table).forEach(item -> {
			
			InfinispanEntry<K, V> infinispanEntry = requestFactory.getItemTransformer().toInfinispanEntry(item);
			
			InternalMetadata metadata = infinispanEntry.getMetadata();
			
			if (metadata == null) {
				return; // no metadata found
			}
			
			if (metadata.isExpired(now)) {
				// Mark item for deletion
				forDeletion.add(infinispanEntry);
			}
		});

		final AtomicInteger deleteCounter = new AtomicInteger(0);
		
		// Delete batching will not work here, doesn't support
		// attribute retrieval to confirm deletion

		for (InfinispanEntry<K,V> en: forDeletion) {

			final boolean deleted = table.deleteItem(requestFactory.resolveDeleteItemSpec(en.getKey())).getItem() != null;
			
			if (deleted) {
				// Notify listener, interested in the Infinispan entry
				MarshalledEntry<K,V> marshalledEntry =
					marshalledEntryFactory.newMarshalledEntry(
						en.getKey(),
						en.getValue(),
						en.getMetadata()
					);
				purgeListener.marshalledEntryPurged(marshalledEntry);
				deleteCounter.incrementAndGet();
			}
		}
		
		Loggers.DYNAMODB_LOG.debug("[DS0128] DynamoDB store: Purged {} expired {} cache entries", deleteCounter.get(), table.getTableName());
		
		return deleteCounter.get();
	}
}
