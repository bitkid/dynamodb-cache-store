/**
 * DynamoDB query executor interfaces.
 */
package com.nimbusds.infinispan.persistence.dynamodb.query;