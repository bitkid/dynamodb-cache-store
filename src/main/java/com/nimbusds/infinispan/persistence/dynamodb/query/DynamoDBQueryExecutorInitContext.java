package com.nimbusds.infinispan.persistence.dynamodb.query;


import com.amazonaws.services.dynamodbv2.document.Index;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.nimbusds.infinispan.persistence.dynamodb.DynamoDBItemTransformer;


/**
 * DynamoDB query executor initialisation context.
 */
public interface DynamoDBQueryExecutorInitContext<K, V> {
	
	
	/**
	 * Returns the configured DynamoDB item transformer.
	 *
	 * @return The DynamoDB item transformer.
	 */
	DynamoDBItemTransformer<K, V> getDynamoDBItemTransformer();
	
	
	/**
	 * Returns the DynamoDB table.
	 *
	 * @return The DynamoDB table.
	 */
	Table getDynamoDBTable();
	
	
	/**
	 * Returns the DynamoDB index for the specified attribute name.
	 *
	 * @param attributeName The attribute name.
	 *
	 * @return The DynamoDB index, {@code null} if not found.
	 */
	Index getDynamoDBIndex(final String attributeName);
}
